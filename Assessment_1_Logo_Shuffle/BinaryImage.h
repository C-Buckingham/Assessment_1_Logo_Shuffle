class BinaryImage : public Matrix{ //BinaryImage is the child of Matrix and can use some of the functions and variables of Matrix
public:
	//Declaring the BinaryImage function to allow for the conversion of the input data
	BinaryImage(int M, int N, double* input_data, double thresh);

	//Binaryimage destructor
	~BinaryImage(){ std::cout << "BinaryImage destructor called" << std::endl; };

};
